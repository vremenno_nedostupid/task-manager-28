package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.ProjectDTO;
import ru.fedun.tm.entity.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService extends IService<Project> {

    void createProject(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    void updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws Exception;

    void updateStartDate(@NotNull String userId, @NotNull String id, @NotNull Date date) throws Exception;

    void updateCompleteDate(@NotNull String userId, @NotNull String id, @NotNull Date date) throws Exception;

    @NotNull
    Long countAllProjects();

    @NotNull
    Long countUserProjects(@NotNull String userId) throws Exception;

    @NotNull
    Project findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull
    Project findOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId) throws Exception;

    void removeOneById(@NotNull String userId, @NotNull String id) throws Exception;

    void removeOneByName(@NotNull String userId, @NotNull String name) throws Exception;

    void removeAll();

    void removeAllByUserId(@NotNull String userId) throws Exception;

}
