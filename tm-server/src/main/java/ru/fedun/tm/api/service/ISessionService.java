package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.dto.UserDTO;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@NotNull String login, @NotNull String password) throws Exception;

    boolean isValid(@NotNull SessionDTO sessionDTO);

    void validate(@NotNull SessionDTO sessionDTO) throws Exception;

    void validate(@NotNull SessionDTO sessionDTO, @NotNull Role role) throws Exception;

    @Nullable
    SessionDTO open(@NotNull String login, @NotNull String password) throws Exception;

    @Nullable
    String createSignature(@NotNull SessionDTO sessionDTO);

    @NotNull
    UserDTO getUser(@NotNull SessionDTO sessionDTO) throws Exception;

    @NotNull
    String getUserId(@NotNull SessionDTO sessionDTO) throws Exception;

    @NotNull
    List<SessionDTO> getSessionList(@NotNull SessionDTO sessionDTO) throws Exception;

    void close(@NotNull SessionDTO sessionDTO) throws Exception;

    void closeAll(@NotNull SessionDTO sessionDTO) throws Exception;

    void signOutByLogin(@NotNull String login) throws Exception;

    void signOutByUserId(@NotNull String userId) throws Exception;

}

