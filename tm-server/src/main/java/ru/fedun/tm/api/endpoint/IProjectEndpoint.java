package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.dto.ProjectDTO;
import ru.fedun.tm.dto.SessionDTO;

import java.util.List;

public interface IProjectEndpoint {

    void createProject(@NotNull SessionDTO session, @NotNull String name, @NotNull String description) throws Exception;

    void clearProjects(@NotNull SessionDTO session) throws Exception;

    @NotNull
    List<ProjectDTO> showAllProjects(@NotNull SessionDTO session) throws Exception;

    @NotNull
    ProjectDTO showProjectById(@NotNull SessionDTO session, @NotNull String id) throws Exception;

    @NotNull
    ProjectDTO showProjectByName(@NotNull SessionDTO session, @NotNull String name) throws Exception;

    void updateProjectById(
            @NotNull SessionDTO session,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    void removeProjectById(@NotNull SessionDTO session, @NotNull String id) throws Exception;

    void removeProjectByName(@NotNull SessionDTO session, @NotNull String name) throws Exception;

}
