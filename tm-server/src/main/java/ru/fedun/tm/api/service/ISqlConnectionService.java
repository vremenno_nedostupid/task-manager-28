package ru.fedun.tm.api.service;

import javax.persistence.EntityManager;

public interface ISqlConnectionService {

    EntityManager getEntityManager();

}

