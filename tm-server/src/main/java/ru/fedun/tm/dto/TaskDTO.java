package ru.fedun.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Task;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class TaskDTO extends AbstractEntityDTO implements Serializable {

    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    private String userId;

    @NotNull
    private String projectId;

    @NotNull
    private Date startDate;

    @NotNull
    private Date completeDate;

    @NotNull
    private Date creationDate;

    public TaskDTO(@NotNull final Task task) {
        id = task.getId();
        title = task.getName();
        description = task.getDescription();
        userId = task.getUser().getId();
        projectId = task.getProject().getId();
        creationDate = task.getCreationDate();
        startDate = task.getStartDate();
        completeDate = task.getCompleteDate();
    }

    @NotNull
    public static TaskDTO toDTO(@NotNull final Task task) {
        return new TaskDTO(task);
    }

    @NotNull
    public static List<TaskDTO> toDTO(@NotNull final Collection<Task> tasks) {
        if (tasks.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            result.add(new TaskDTO(task));
        }
        return result;
    }

    @Override
    public String toString() {
        return getTitle();
    }

}

