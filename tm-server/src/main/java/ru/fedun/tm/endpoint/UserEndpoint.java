package ru.fedun.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.IUserEndpoint;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.dto.SessionDTO;
import ru.fedun.tm.dto.UserDTO;
import ru.fedun.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint implements IUserEndpoint {

    private ServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "password", partName = "password") @NotNull final String password,
            @WebParam(name = "newPassword", partName = "newPassword") @NotNull final String newPassword
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updatePassword(session.getUserId(), newPassword);
    }

    @Override
    @WebMethod
    public void updateMail(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "newEmail", partName = "newEmail") @NotNull final String newEmail
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateMail(session.getUserId(), newEmail);
    }

    @NotNull
    @Override
    @WebMethod
    public UserDTO showUserProfile(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getUser(session);
    }

}
