package ru.fedun.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public final class Session extends AbstractEntity {

    @NotNull
    @Column(columnDefinition = "TEXT",
            updatable = false)
    private Long startTime;

    @Nullable
    @Column(columnDefinition = "TEXT",
            updatable = false)
    private String signature;

    @NotNull
    @ManyToOne
    private User user;

}

