package ru.fedun.tm.exception.notfound;

import ru.fedun.tm.exception.AbstractRuntimeException;

public final class UserNotFoundException extends AbstractRuntimeException {

    private final static String message = "Error! User not found...";

    public UserNotFoundException() {
        super(message);
    }
}
