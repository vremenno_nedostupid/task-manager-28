package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyFirstNameException extends AbstractRuntimeException {

    private static final String message = "Error! First name is empty...";

    public EmptyFirstNameException() {
        super(message);
    }

}
