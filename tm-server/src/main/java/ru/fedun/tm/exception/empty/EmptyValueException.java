package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyValueException extends AbstractRuntimeException {

    private final static String message = "Error! Value is empty...";

    public EmptyValueException() {
        super(message);
    }

}
