package ru.fedun.tm.exception.empty;

import ru.fedun.tm.exception.AbstractRuntimeException;

public class EmptyMiddleNameException extends AbstractRuntimeException {

    private final static String message = "Error! Middle name is empty...";

    public EmptyMiddleNameException() {
        super(message);
    }

}
