package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.service.IService;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.entity.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final ServiceLocator serviceLocator;

    public AbstractService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
