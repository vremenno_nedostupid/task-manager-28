package ru.fedun.tm.command.project.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectRemoveByTitleCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-title";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by title.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER TITLE:]");
        @NotNull final String title = TerminalUtil.nextLine();
        if (title.isEmpty()) {
            System.out.println("[FAIL]");
            return;
        }
        serviceLocator.getProjectEndpoint().removeProjectByName(session, title);
        System.out.println("[OK]");
        System.out.println();
    }

}
