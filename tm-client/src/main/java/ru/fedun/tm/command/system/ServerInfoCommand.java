package ru.fedun.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;

public class ServerInfoCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Show info about server.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SERVER INFO]");
        @NotNull SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
        @NotNull Integer serverPort = serviceLocator.getSessionEndpoint().getServerPort(sessionDTO);
        @NotNull String serverHost = serviceLocator.getSessionEndpoint().getServerHost(sessionDTO);
        System.out.println("Server host:" + serverHost);
        System.out.println("Server port:" + serverPort);
        System.out.println("[OK]");
    }

}
