package ru.fedun.tm.command.task.show;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.SessionDTO;
import ru.fedun.tm.endpoint.Task;
import ru.fedun.tm.endpoint.TaskDTO;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskShowByTitleCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-title";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER TITLE:]");
        @NotNull final String title = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = serviceLocator.getTaskEndpoint().findTaskByName(session, title);
        System.out.println("ID: " + task.getId());
        System.out.println("TITLE: " + task.getTitle());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
